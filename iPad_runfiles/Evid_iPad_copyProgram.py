#!/usr/bin/env python


from docx import *
import time ,os,sys,datetime, string
import fnmatch,shutil
from PIL import Image

def image_compress(working_dir,changed_file):
       print "compress"
       for i in range(0,len(changed_file)):
              print changed_file[i]
              print working_dir
              data = (working_dir + '/' + changed_file[i])
              print data
              im = Image.open(data)
              print "done"
              im = im.resize((160,300),Image.ANTIALIAS)
              im.save(data,quality=95)
       
def del_temp_media(working_dir):
       mediafolder = "%s/template/word/media" %working_dir
       for r, d, f in os.walk(mediafolder):
              for name in f:
                     os.remove(os.path.join(mediafolder, name))
       print "Temp Folder Cleared..!!"
       

def copy_images(working_dir,img_dir,img_names):
       rootDirs = img_names
       pattern_find = img_names[1]
       #print img_names
       imgList = []
       alist_filter = ['jpg','bmp','png','gif','PNG'] 
       for r,d,f in os.walk(img_dir):
           for file in f:
                  changed_file = file.replace(" ", "")
                  if file[-3:] in alist_filter:
                         sourceFolder = os.path.join(img_dir,file)
                         shutil.copy(sourceFolder,working_dir)
                         os.renames(working_dir + '/' + file, working_dir + '/' + changed_file)
                         imgList.append(changed_file)
       #os.remove(working_dir+"/Evid_iPhone_Module.docx")
       return imgList

def del_images(working_dir,img_names,indexNumb):
    print "Delete"
    print img_names
    for i in range(indexNumb,len(img_names)):
        print img_names[i]
        img_del_path = "%s/%s" %(working_dir,img_names[i])
        os.remove(img_del_path)

def result_Move(working_dir1,DocFileName):
       try:
              oldResult = working_dir1 + '/Result Evid/' + DocFileName
              os.remove(oldResult)
       except:
              print "No Files in Result Folder.. Going Forward"
       shutil.move(working_dir1 + '/' + DocFileName,working_dir1 + '/Result Evid')
       print "Moved File.."
        
if __name__ == '__main__':
    # Default set of relationshipships - the minimum components of a document
 
    relationships = relationshiplist()
    print relationships

    # Make a new document tree - this is the main part of a Word document
    document = newdocument()

    working_dir1 = os.getcwd()
    working_dir = working_dir1 + '/iPad_Runfiles'
    #print working_dir
    rootDirPath =  os.path.dirname(working_dir)
    #Del Temp Folder files
    del_temp_media(working_dir)
    #print rootDirPath
    img_dir = rootDirPath +'/iPad_Screen'
    #numbOfImgs = len(os.walk(img_dir).next()[2])
    img_names = os.listdir(img_dir)
    print img_names
    print "WK",working_dir
    change_imgNames = copy_images(working_dir,img_dir,img_names)
    #image_compress(working_dir,change_imgNames)
    #
    indexNumb = 0
    if(change_imgNames[0] == '.DS_Store'):
           indexNumb = 1
           print "Index is 1"
    else:
           print "indexNumb is ",indexNumb
    new_lineIndex = 0
    for i in range (indexNumb,len(change_imgNames)):
        #print img_names[i]
        imgPath = img_dir + "/" + change_imgNames[i]
        print "IMG",imgPath
        xImageDir = working_dir + '/' + change_imgNames[i]
        print "Img Dir : ",xImageDir
        im = Image.open(xImageDir)
        img_size = im.size
        img_Flag = 0
        if((img_size[0] == 1536) or (img_size[0] == 2048)):
               if((img_size[0] == 2048) or (img_size[0] == 1536)):
                      img_Flag = True
                      
        if(img_Flag == True):
               print "Device : iPad 4"
        else:
               print "Device : non iPad 4"
        
        # This xpath location is where most interesting content lives
        body = document.xpath('/w:document/w:body', namespaces=nsprefixes)[0]
        #z = '/Users/ZA028309/Desktop/Copy_images/runfiles/IMG_1360.jpg'
        # Add an image
        relationships, picpara = picture(relationships, xImageDir,change_imgNames[i],
                                     'Image description',img_Flag)
        if(new_lineIndex == 0):
               body.append(heading("/n",0))
               new_lineIndex = 1
               body.append(heading("iPad",0))
        else:
               body.append(heading("",0))
       
        body.append(picpara)
        # Add a pagebreak
        body.append(pagebreak(type='page', orient='portrait'))

    # Create our properties, contenttypes, and other support files
    title    = 'Python docx'
    subject  = 'A practical example of making docx from Python'
    creator  = 'Zaheer'
    keywords = ['python', 'Office Open XML', 'Word']

    coreprops = coreproperties(title=title, subject=subject, creator=creator,
                               keywords=keywords)
    appprops = appproperties()
    contenttypes = contenttypes()
    websettings = websettings()
    wordrelationships = wordrelationships(relationships)

    DocFileName = 'Evid_iPad_Module.docx'

    # Save our document
    savedocx(document, coreprops, appprops, contenttypes, websettings,
             wordrelationships, DocFileName)
    print "DOCUMENT CREATED.."
    result_Move(working_dir1,DocFileName)
    time.sleep(1)
    del_images(working_dir,change_imgNames,indexNumb)
    print "Run Sucesfull.!!!"

    
